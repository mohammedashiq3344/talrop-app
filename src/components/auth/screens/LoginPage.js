import React from 'react';
import {View, Text, Image, TextInput, TouchableOpacity} from 'react-native';
import styled from 'styled-components';
import LogoComponent from '../includes/LogoComponent';
import Footer from '../includes/Footer';

function LoginPage() {
  return (
    <>
      <LogoComponent />
      <LoginText>Login to your account</LoginText>
      <MobRequestText>Enter your registered mobile phone number</MobRequestText>
      <MiddleContainer>
        <FlagsContainer>
          <DefaultFlag source={require('../../../images/flag.png')} />
        </FlagsContainer>
        <NumberContainer>
          <DefaultCode>+91</DefaultCode>
          <InputContainer
            type="text"
            placeholder="Enter your phone number"></InputContainer>
        </NumberContainer>
      </MiddleContainer>
      <NextButton>Next</NextButton>
      <NewReg>
        <Question>New to Talrop?</Question>
        <RegLink to="#">Create an account</RegLink>
      </NewReg>
      <Footer />
    </>
  );
}

const LoginText = styled(Text)`
  margin-top: 25px;
  font-family: 'BalooPaaji2-Medium';
  font-size: 25px;
`;
const MobRequestText = styled(Text)`
  font-size: 15px;
  margin-top: 7px;
  color: #868686;
  font-family: 'BalooPaaji2-Medium';
  margin-bottom: 40px;
`;
const MiddleContainer = styled(View)`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
`;
const FlagsContainer = styled(View)`
  border-radius: 50px;
  width: 13%;
`;
const DefaultFlag = styled(Image)`
  height: 46px;
  width: 46px;
  border-radius: 50px;
`;
const NumberContainer = styled(View)`
  border: 1px solid #868686;
  border-radius: 7px;
  display: flex;
  flex-direction: row;
  align-items: center;
  width: 82%;
`;
const NextButton = styled(Text)`
  background: rgb(92, 198, 106);
  border-radius: 6px;
  display: flex;
  padding: 15px 140px;
  justify-content: center;
  margin-top: 40px;
  color: #fff;
`;
const InputContainer = styled(TextInput)`
  width: 100%;
`;
const DefaultCode = styled(Text)`
  margin-right: 1px;
  margin-left: 10px;
  font-weight: bold;
`;
const NewReg = styled(View)`
  display: flex;
  flex-direction: row;
  margin: 20px 0px 100px;
  justify-content: center;
`;
const Question = styled(Text)`
  margin-right: 10px;
  font-family: 'BalooPaaji2-Regular';
`;
const RegLink = styled(Text)`
  color: rgb(92, 198, 106);
  font-size: 14px;
  font-family: 'BalooPaaji2-Regular';
`;

export default LoginPage;
