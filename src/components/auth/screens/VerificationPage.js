import React from 'react';
import {View, Text, Image, TextInput, TouchableOpacity} from 'react-native';
import styled from 'styled-components';
import Footer from '../includes/Footer';
import LogoComponent from '../includes/LogoComponent';

function VerificationPage() {
  return (
    <>
      <LogoComponent />
      <PasswordText>Password</PasswordText>
      <PasswordRequestText>
        Enter your Password for this account
      </PasswordRequestText>
      <MiddleContainer>
        <NumberContainer>
          <LockContainer>
            <LockImage source={require('../../../images/lock.png')} />
          </LockContainer>
          <Form>
            <InputContainer
              type="password"
              placeholder="Enter your password"></InputContainer>
          </Form>
          <EyeContainer>
            <EyeImage source={require('../../../images/eye.png')} />
          </EyeContainer>
        </NumberContainer>
        <LoginWithOtp>Login with OTP</LoginWithOtp>
      </MiddleContainer>
      <LoginButton>Login</LoginButton>
      <Forgot>
        <ForgotLink to="#">Forgot password</ForgotLink>
      </Forgot>
      <Footer />
    </>
  );
}

const PasswordText = styled(Text)`
  margin-top: 25px;
  font-family: 'BalooPaaji2-Medium';
  font-size: 25px;
`;
const PasswordRequestText = styled(Text)`
  font-size: 15px;
  margin-top: 7px;
  color: #868686;
  font-family: 'BalooPaaji2-Medium';
  margin-bottom: 40px;
`;
const MiddleContainer = styled(View)`
  display: flex;
  flex-direction: column;
`;
const NumberContainer = styled(View)`
  border: 1px solid #868686;
  border-radius: 7px;
  display: flex;
  flex-direction: row;
  align-items: center;
  width: 100%;
  padding: 2px;
`;
const LockContainer = styled(View)`
  /* width: 30px;
  height: 30px; */
  margin-left: 10px;
  margin-right: 10px;
  display: flex;
  align-items: center;
`;
const LockImage = styled(Image)`
  width: 12.91px;
  height: 17px;
`;
const EyeContainer = styled(View)`
  display: flex;
  justify-content: flex-end;
`;
const EyeImage = styled(Image)`
  width: 17px;
  height: 10.83px;
`;
const LoginWithOtp = styled(Text)`
  display: flex;
  text-align: right;
  font-size: 13px;
  font-family: 'BalooPaaji2-Medium';
`;
const LoginButton = styled(Text)`
  background: rgb(92, 198, 106);
  border-radius: 6px;
  display: flex;
  padding: 15px 140px;
  justify-content: center;
  margin-top: 40px;
  color: #fff;
`;
const Form = styled(View)``;
const InputContainer = styled(TextInput)`
  margin-right: 125px;
`;
const Forgot = styled(View)`
  display: flex;
  flex-direction: row;
  margin: 20px 0px 100px;
  justify-content: center;
`;
const ForgotLink = styled(Text)`
  color: rgb(92, 198, 106);
  font-size: 14px;
  font-family: 'BalooPaaji2-Regular';
`;

export default VerificationPage;
