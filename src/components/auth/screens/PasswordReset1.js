import React from 'react';
import {View, Text, Image, TextInput, TouchableOpacity} from 'react-native';
import styled from 'styled-components';
import Footer from '../includes/Footer';
import LogoComponent from '../includes/LogoComponent';

function PasswordReset1() {
  return (
    <>
      <LogoComponent />
      <ResetText>Password Reset setup (1/3)</ResetText>
      <ResetRequestText>
        Enter your registered mobile phone number to reset your password
      </ResetRequestText>
      <MiddleContainer>
        <FlagsContainer>
          <DefaultFlag source={require('../../../images/flag.png')} />
        </FlagsContainer>
        <NumberContainer>
          <DefaultCode>+91</DefaultCode>
          <InputContainer
            type="text"
            placeholder="Enter your phone number"></InputContainer>
        </NumberContainer>
      </MiddleContainer>
      <SendButton>Send OTP</SendButton>
      <Footer />
    </>
  );
}

const ResetText = styled(Text)`
  margin-top: 25px;
  font-family: 'BalooPaaji2-Medium';
  font-size: 25px;
`;
const ResetRequestText = styled(Text)`
  font-size: 15px;
  margin-top: 7px;
  color: #868686;
  font-family: 'BalooPaaji2-Medium';
  margin-bottom: 40px;
`;
const MiddleContainer = styled(View)`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
`;
const FlagsContainer = styled(View)`
  border-radius: 50px;
  width: 13%;
`;
const DefaultFlag = styled(Image)`
  height: 46px;
  width: 46px;
  border-radius: 50px;
`;
const NumberContainer = styled(View)`
  border: 1px solid #868686;
  border-radius: 7px;
  display: flex;
  flex-direction: row;
  align-items: center;
  width: 82%;
`;
const SendButton = styled(Text)`
  background: rgb(92, 198, 106);
  border-radius: 6px;
  text-align: center;
  padding: 15px 100px;
  margin-top: 40px;
  color: #fff;
  margin-bottom: 137px;
`;
const InputContainer = styled(TextInput)`
  width: 100%;
`;
const DefaultCode = styled(Text)`
  margin-right: 1px;
  margin-left: 10px;
  font-weight: bold;
`;

export default PasswordReset1;
