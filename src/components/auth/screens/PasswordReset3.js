import React from 'react';
import {View, Text, Image, TextInput, TouchableOpacity} from 'react-native';
import styled from 'styled-components';
import Footer from '../includes/Footer';
import LogoComponent from '../includes/LogoComponent';

function PasswordReset3() {
  return (
    <>
      <LogoComponent />
      <PasswordText>Password Reset Setup (3/3)</PasswordText>
      <PasswordRequestText>
        Set a strong password for your account
      </PasswordRequestText>
      <MiddleContainer>
        <NumberContainer>
          <LockContainer>
            <LockImage source={require('../../../images/lock.png')} />
          </LockContainer>
          <Form>
            <InputContainer
              type="password"
              placeholder="Enter your password"></InputContainer>
          </Form>
          <EyeContainer>
            <EyeImage source={require('../../../images/eye.png')} />
          </EyeContainer>
        </NumberContainer>
        <NumberContainer>
          <LockContainer>
            <LockImage source={require('../../../images/lock.png')} />
          </LockContainer>
          <Form>
            <ConfirmInputContainer
              type="password"
              placeholder="Confirm your password"></ConfirmInputContainer>
          </Form>
          <EyeContainer>
            <EyeImage source={require('../../../images/eye.png')} />
          </EyeContainer>
        </NumberContainer>
      </MiddleContainer>
      {/* <ValidationContainer>
        <ConditionContainer>
          <RoundIcon source={require('../../../images/icon-round.png')} />
          <Condition>Should contain at least 8 characters</Condition>
        </ConditionContainer>
        <ConditionContainer>
          <RoundIcon source={require('../../../images/icon-round.png')} />
          <Condition>
            Should contain a lowercase (small) letter (a -z)
          </Condition>
        </ConditionContainer>
        <ConditionContainer>
          <RoundIcon source={require('../../../images/icon-round.png')} />
          <Condition>
            Should contain a uppercase (capital) letter (A - Z)
          </Condition>
        </ConditionContainer>
        <ConditionContainer>
          <RoundIcon source={require('../../../images/icon-round.png')} />
          <Condition>Should contain at least one number (0-9)</Condition>
        </ConditionContainer>
        <ConditionContainer>
          <RoundIcon source={require('../../../images/icon-round.png')} />
          <Condition>
            Should contain at least one symbol ($,@,#,%,!,*,?,&)
          </Condition>
        </ConditionContainer>
      </ValidationContainer> */}
      <ResetButton>Reset</ResetButton>
      <Footer />
    </>
  );
}

const PasswordText = styled(Text)`
  margin-top: 25px;
  font-family: 'BalooPaaji2-Medium';
  font-size: 25px;
`;
const PasswordRequestText = styled(Text)`
  font-size: 15px;
  margin-top: 7px;
  color: #868686;
  font-family: 'BalooPaaji2-Medium';
  margin-bottom: 40px;
`;
const MiddleContainer = styled(View)`
  display: flex;
  flex-direction: column;
`;
const NumberContainer = styled(View)`
  border: 1px solid #868686;
  border-radius: 7px;
  display: flex;
  flex-direction: row;
  align-items: center;
  width: 100%;
  padding: 2px;
  margin-bottom: 15px;
`;
const LockContainer = styled(View)`
  /* width: 30px;
  height: 30px; */
  margin-left: 10px;
  margin-right: 10px;
  display: flex;
  align-items: center;
`;
const LockImage = styled(Image)`
  width: 12.91px;
  height: 17px;
`;
const EyeContainer = styled(View)`
  display: flex;
`;
const EyeImage = styled(Image)`
  width: 17px;
  height: 10.83px;
`;

const ResetButton = styled(Text)`
  background: rgb(92, 198, 106);
  border-radius: 6px;
  display: flex;
  padding: 15px 140px;
  justify-content: center;
  margin-top: 40px;
  color: #fff;
  margin-bottom: 80px;
`;
const Form = styled(View)``;
const InputContainer = styled(TextInput)`
  margin-right: 125px;
`;
const ConfirmInputContainer = styled(TextInput)`
  margin-right: 110px;
`;
// const ValidationContainer = styled(View)``;
// const ConditionContainer = styled(View)`
//   display: flex;
//   flex-direction: row;
//   align-items: center;
//   margin-bottom: 5px;
// `;
// const RoundIcon = styled(Image)`
//   width: 15px;
//   height: 15px;
//   margin-right: 8px;
// `;
// const Condition = styled(Text)`
//   font-family: 'BalooPaaji2-Medium';
//   color: rgb(134, 134, 134);
// `;

export default PasswordReset3;
