import React from 'react';
import {View, Text, Image, TextInput, TouchableOpacity} from 'react-native';
import styled from 'styled-components';
import Footer from '../includes/Footer';
import LogoComponent from '../includes/LogoComponent';

function PasswordReset2() {
  return (
    <>
      <LogoComponent />
      <PasswordText>Password Reset Setup (2/3)</PasswordText>
      <PasswordRequestText>
        Enter the four digit OTP sent to the registered mobile number{' '}
      </PasswordRequestText>
      <MiddleContainer>
        <NumberContainer>
          <LockContainer>
            <LockImage source={require('../../../images/phone.png')} />
          </LockContainer>
          <Form>
            <InputContainer
              type="password"
              placeholder="Enter OTP"></InputContainer>
          </Form>
        </NumberContainer>
        <LoginWithOtp>Resend OTP</LoginWithOtp>
      </MiddleContainer>
      <Watsapp>
        <Question>
          If you are facing any issues to get the OTP, kindly Whatsapp to
          <Number to="#"> +91 858 9999 552</Number>
        </Question>
      </Watsapp>
      <VerifyButton>Verify</VerifyButton>
      <Footer />
    </>
  );
}

const PasswordText = styled(Text)`
  margin-top: 25px;
  font-family: 'BalooPaaji2-Medium';
  font-size: 25px;
`;
const PasswordRequestText = styled(Text)`
  font-size: 15px;
  margin-top: 7px;
  color: #868686;
  font-family: 'BalooPaaji2-Medium';
  margin-bottom: 40px;
`;
const MiddleContainer = styled(View)`
  display: flex;
  flex-direction: column;
`;
const NumberContainer = styled(View)`
  border: 1px solid #868686;
  border-radius: 7px;
  display: flex;
  flex-direction: row;
  align-items: center;
  width: 100%;
  padding: 2px;
`;
const LockContainer = styled(View)`
  margin-left: 10px;
  margin-right: 10px;
  display: flex;
  align-items: center;
`;
const LockImage = styled(Image)`
  width: 13px;
  height: 20px;
`;

const LoginWithOtp = styled(Text)`
  display: flex;
  text-align: right;
  font-size: 13px;
  font-family: 'BalooPaaji2-Medium';
`;
const LoginButton = styled(Text)`
  background: rgb(92, 198, 106);
  border-radius: 6px;
  display: flex;
  padding: 15px 140px;
  justify-content: center;
  margin-top: 40px;
  color: #fff;
`;
const Form = styled(View)``;
const InputContainer = styled(TextInput)`
  margin-right: 110px;
`;
const Watsapp = styled(View)`
  margin: 20px 0px;
`;
const Question = styled(Text)`
  font-family: 'BalooPaaji2-Regular';
`;
const Number = styled(Text)`
  color: rgb(92, 198, 106);
  font-size: 14px;
  font-family: 'BalooPaaji2-Regular';
`;
const VerifyButton = styled(Text)`
  background: rgb(92, 198, 106);
  border-radius: 6px;
  display: flex;
  padding: 15px 140px;
  color: #fff;
  margin-bottom: 70px;
`;

export default PasswordReset2;
