import React from 'react';
import {View, Text, Image, TextInput, TouchableOpacity} from 'react-native';
import styled from 'styled-components';
import Footer from '../includes/Footer';
import LogoComponent from '../includes/LogoComponent';

function NewAccount() {
  return (
    <>
      <LogoComponent />
      <TitleText>
        Join Talrop! A digital education platform for Industry 4.0.
      </TitleText>
      {/* <IntroductionText>
        Discover the Industry 4.0 courses to upskill yourselves from our top
        experts and gear up for the next-generation revolution.
      </IntroductionText> */}
      <MiddleContainer>
        <FlagsContainer>
          <DefaultFlag source={require('../../../images/flag.png')} />
        </FlagsContainer>
        <NumberContainer>
          <DefaultCode>+91</DefaultCode>
          <InputContainer
            type="text"
            placeholder="Enter your phone number"></InputContainer>
        </NumberContainer>
      </MiddleContainer>
      <JoinButton>Join</JoinButton>
      <NewReg>
        <Question>Already have an account?</Question>
        <LoginLink to="#">Login</LoginLink>
      </NewReg>
      <Footer />
    </>
  );
}

const Logo = styled(Image)`
  width: 75px;
  height: 75px;
`;
const TitleText = styled(Text)`
  margin-top: 25px;
  font-family: 'BalooPaaji2-Medium';
  font-size: 25px;
  line-height: 30;
  margin-bottom: 60px;
`;
// const IntroductionText = styled(Text)`
//   font-size: 15px;
//   margin-top: 7px;
//   color: #868686;
//   font-family: 'BalooPaaji2-Medium';
//   margin-bottom: 40px;
// `;
const MiddleContainer = styled(View)`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
`;
const FlagsContainer = styled(View)`
  border-radius: 50px;
  width: 13%;
`;
const DefaultFlag = styled(Image)`
  height: 46px;
  width: 46px;
  border-radius: 50px;
`;
const NumberContainer = styled(View)`
  border: 1px solid #868686;
  border-radius: 7px;
  display: flex;
  flex-direction: row;
  align-items: center;
  width: 82%;
`;
const JoinButton = styled(Text)`
  background: rgb(92, 198, 106);
  border-radius: 6px;
  display: flex;
  padding: 15px 140px;
  justify-content: center;
  margin-top: 40px;
  color: #fff;
`;
const InputContainer = styled(TextInput)`
  width: 100%;
`;
const DefaultCode = styled(Text)`
  margin-right: 1px;
  margin-left: 10px;
  font-weight: bold;
`;
const NewReg = styled(View)`
  display: flex;
  flex-direction: row;
  margin: 20px 0px 50px;
  justify-content: center;
`;
const Question = styled(Text)`
  margin-right: 10px;
  font-family: 'BalooPaaji2-Regular';
`;
const LoginLink = styled(Text)`
  color: rgb(92, 198, 106);
  font-size: 14px;
  font-family: 'BalooPaaji2-Regular';
`;

export default NewAccount;
