import React from 'react';
import {View, Text, Image, TextInput, TouchableOpacity} from 'react-native';
import styled from 'styled-components';

function Footer() {
  return (
    <>
      <FooterDiv>
        <FooterText>Terms of Services</FooterText>
      </FooterDiv>
    </>
  );
}
const FooterDiv = styled(View)`
  border-top-width: 1px;
  border-top-color: #868686;
  width: 100%;
  display: flex;
  align-items: center;
  padding: 22px 0px;
`;
const FooterText = styled(Text)`
  font-family: 'BalooPaaji2-Bold';
  color: #868686;
`;

export default Footer;
