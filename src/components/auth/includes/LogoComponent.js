import React from 'react';
import {Image} from 'react-native';
import styled from 'styled-components';

function LogoComponent() {
  return (
    <>
      <Logo source={require('../../../images/talrop-color-logo.png')} />
    </>
  );
}

const Logo = styled(Image)`
  width: 80px;
  height: 80px;
`;
export default LogoComponent;
