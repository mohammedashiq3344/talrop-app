import React from 'react';
import {Text, View, Form, Image} from 'react-native';
import {TextInput} from 'react-native-gesture-handler';
import styled from 'styled-components';

const Country = () => {
  return (
    <BackgroundContainer>
      <MainContainer>
        <SearchBar>
          <ImageContainer>
            <Icon source={require('../../../images/search.png')} />
          </ImageContainer>
          <InputArea placeholder="Search Here..."></InputArea>
        </SearchBar>
        <CountryContainer>
          <FlagContainer>
            <Flag source={require('../../../images/flag.png')} />
          </FlagContainer>
          <CountryCodeContainer>
            <CountryCode>India(+91)</CountryCode>
            <BadgeContainer>
              <BadgeIcon source={require('../../../images/checked.png')} />
            </BadgeContainer>
          </CountryCodeContainer>
        </CountryContainer>
      </MainContainer>
    </BackgroundContainer>
  );
};

export default Country;

const BackgroundContainer = styled(View)`
  background: rgba(0, 0, 0, 0.5);
  position: absolute;
  width: 100%;
  z-index: 9999;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  display: flex;
  justify-content: center;
  padding: 0 30px;
`;
const MainContainer = styled(View)`
  background-color: #fff;
  padding: 20px 15px;
  border-radius: 7px;
  min-height: 400px;
`;
const SearchBar = styled(View)`
  width: 100%;
  border: 1px solid rgb(218, 227, 237);
  padding: 10px 12px;
  border-radius: 4px;
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
`;
const ImageContainer = styled(View)`
  width: 10%;
`;
const Icon = styled(Image)`
  height: 18px;
  width: 17.98px;
`;
const InputArea = styled(TextInput)`
  width: 90%;
  padding: 0;
  font-size: 16px;
  font-family: 'BalooPaaji2-Medium';
  &:placeholder {
    font-family: BalooPaaji2;
  }
`;
const CountryContainer = styled(View)`
  width: 100%;
  display: flex;
  justify-content: center;
  flex-direction: row;
  align-items: center;
  padding: 15px 10px;
`;
const FlagContainer = styled(View)`
  width: 18%;
`;
const Flag = styled(Image)`
  width: 37px;
  height: 25px;
`;
const CountryCodeContainer = styled(View)`
  width: 82%;
  display: flex;
  justify-content: space-between;
  flex-direction: row;
  align-items: center;
`;
const CountryCode = styled(Text)`
  font-family: 'BalooPaaji2-SemiBold';
  font-size: 16px;
  color: #42c870;
`;
const BadgeContainer = styled(View)``;

const BadgeIcon = styled(Image)``;
