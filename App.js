import 'react-native-gesture-handler';
import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import styled from 'styled-components';
import LoginPage from './src/components/auth/screens/LoginPage';
import PasswordReset2 from './src/components/auth/screens/PasswordReset2';
import PasswordReset3 from './src/components/auth/screens/PasswordReset3';
import PasswordReset1 from './src/components/auth/screens/PasswordReset1';
import VerificationPage from './src/components/auth/screens/VerificationPage';
import NewAccount from './src/components/auth/screens/NewAccount';

function App() {
  return (
    <MainContainer>
      <StatusBar barStyle="dark-content" />
      <LoginPage />
      {/* <NewAccount />
     <VerificationPage />
     <PasswordReset1 />
     <PasswordReset2 />
     <PasswordReset3 /> */}
    </MainContainer>
  );
}

const MainContainer = styled(SafeAreaView)`
  display: flex;
  padding: 150px 30px 0 30px;
`;

export default App;
